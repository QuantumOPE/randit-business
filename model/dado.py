# -*- coding: utf-8 -*-
class Dado():
    def __init__(self, data, de, para, open, high, low, close, id=0):
        self.id = id
        self.data = data
        self.de = de
        self.para = para
        self.open = open
        self.high = high
        self.low = low
        self.close = close

    def atualizar(self, dados):
        try:
            id = 0
            data = dados["data"]
            de = dados["de"]
            para = dados["para"]
            open = dados["open"]
            high = dados["high"]
            low = dados["low"]
            close = dados["close"]
            self.id, self.data, self.de, self.para, \
            self.open, self.high, self.low, self.close  = \
                id, data, de, para, open, high, low, close
            return self
        except Exception as e:
            print("Problema ao atualizar novo registro!")
            print(e)

    def __dict__(self):
        d = dict()
        d['id'] = self.id
        d["data"] = self.data
        d["de"] = self.de
        d["para"] = self.para
        d["open"] = self.open
        d["high"] = self.high
        d["low"] = self.low
        d["close"] = self.close
        return d

    @staticmethod
    def cria(dados, dados_data):
        try:
            id = 0
            data = dados_data
            de = dados["Meta Data"]["2. From Symbol"]
            para = dados["Meta Data"]["3. To Symbol"]
            open = dados["Time Series FX (5min)"][dados_data]["1. open"]
            high = dados["Time Series FX (5min)"][dados_data]["2. high"]
            low = dados["Time Series FX (5min)"][dados_data]["3. low"]
            close = dados["Time Series FX (5min)"][dados_data]["4. close"]
            return Dado(data=data, de=de, para=para, open=open, high=high, low=low, close=close, id=id)
        except Exception as e:
            print("Problema ao criar novo registro!")
            print(e)

    @staticmethod
    def cria_de_tupla(registro):
        try:
            id = registro[0]
            data = registro[1]
            de = registro[2]
            para = registro[3]
            open = registro[4]
            high = registro[5]
            low = registro[6]
            close = registro[7]
            return Dado(data=data, de=de, para=para, open=open, high=high, low=low, close=close, id=id)
        except Exception as e:
            print("Problema ao retornar novo registro!")
            print(e)