# -*- coding: utf-8 -*-
import sys
import requests as req
import json
import plotly
import pandas as pd
import numpy as np
from flask import Blueprint, jsonify, request, render_template
from config.lang import pt
from config.currency import currency as moeda

from services.dashboard_service import tax_rates, market, sector, news


sys.path.append("..")

dashboardapp = Blueprint('dashboardapp', __name__)


@dashboardapp.route('/dashboard', methods=['GET'])
def listar():
    try:
        params = request.args.to_dict()
        data = dict()
        data["news"] = news(params)
        data["tax_rates"] = tax_rates(params)
        data["market"] = market(params)
        data["sector"] = sector(params)

        graphs = [
            dict(
                data=[
                    dict(
                        x=[i for i in data["tax_rates"]],
                        y=[float(data["tax_rates"][i]) for i in data["tax_rates"]],
                        type='scatter'
                    ),
                ],
                layout=dict(
                    title=f'Valor {params["from"]} em {params["to"]}'
                )
            ),

            dict(
                data=[
                    dict(
                        x=[i for i in data["market"]],
                        y=[float(data["market"][i]) for i in data["market"]],
                        type='bar'
                    ),
                ],
                layout=dict(
                    title=f'Balanço e valores da {params["stock"]}. (valores em USD)'
                )
            ),

            # dict(
            #     data=[
            #         dict(
            #             x=ts.index,  # Can use the pandas data structures directly
            #             y=ts
            #         )
            #     ]
            # )
        ]
        ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]
        graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)
        return render_template('dashboard.html',
                            ids=ids,
                            graphJSON=graphJSON,
                            sector=data["sector"],
                            letters=data["news"],
                            ptBR=pt,
                            moeda=moeda,
                            )
    except Exception as e:
        return '<h1>Opa, faltou parâmetro ai meu amigo, volta lá e tenta denovo</h1>'
