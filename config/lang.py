pt = {
'Information Technology': 'Tecnologia',
 'Consumer Discretionary': 'Consumo discricionário',
 'Health Care': 'Saúde',
 'Utilities': 'Utilidade Pública',
 'Consumer Staples': 'Consumos básicos',
 'Communication Services': 'Serviços de Comunicação',
 'Industrials': 'Industrias',
 'Financials': 'Finanças',
 'Materials': 'Materiais',
 'Energy': 'Energia',
 'Real Estate': 'Imóveis'
 }
