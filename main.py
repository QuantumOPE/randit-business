# -*- coding: utf-8 -*-
#!/usr/bin/python3
from flask import Flask, jsonify, request, render_template
from data.db_dao import Database
from routes.dashboard_route import dashboardapp

app = Flask(__name__, template_folder='templates',static_folder='templates/styles')
app.register_blueprint(dashboardapp)

@app.route('/', methods=['GET'])
def main():
    # Database.createDB()
    return render_template('index.html')

if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)

    
