# -*- coding: utf-8 -*-
import sys
import requests as req
from config.app_config import CONSTANTS
from model.dado import Dado

sys.path.append("..")

def tax_rates(params):
    response = dict()
    url = f"{CONSTANTS['URL_API']}function=FX_WEEKLY&from_symbol={params['from']}&to_symbol={params['to']}&apikey={CONSTANTS['API_KEY']}"
    data = req.get(url).json()
    for i in data['Time Series FX (Weekly)']:
        response[i] = data['Time Series FX (Weekly)'][i]['4. close']
    return response

def market(params):
    response = dict()
    url = '{}function=TIME_SERIES_WEEKLY&symbol={}&apikey={}'.format(CONSTANTS["URL_API"], params["stock"], CONSTANTS["API_KEY"])
    data = req.get(url).json()
    for i in data['Weekly Time Series']:
        response[i] = data['Weekly Time Series'][i]['4. close']
    return response

def sector(params):
    url = '{}function=SECTOR&&apikey={}'.format(CONSTANTS["URL_API"], CONSTANTS["API_KEY"])
    response = req.get(url).json()
    return response

def news(params):
    url = f'{CONSTANTS["NYT_URL"]}q=stores&api-key={CONSTANTS["NYT_API_KEY"]}'
    response = req.get(url).json()['response']["docs"]
    return response